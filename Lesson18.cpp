﻿#include <iostream>
//подключение для функции sort
#include <algorithm>   
using namespace std;

class Player
{
    string name;
    int points;

public:
    friend bool point_sorter(const Player&, const Player&); //дает доступ к одноименному методу ниже

    string get_name()                            //получение из массива имени           
    {
        return name;
    }

    int get_points()                            //получение из массива очков
    {
        return points;
    }

    void set_name(string n)                     //запись в ячейку массива имени 
    {
        name = n;
    }

    void set_points(int n)                      //запись в ячейку массива очков 
    {
        points = n;
    }

};

bool point_sorter(const Player& l, const Player& r) // сортировка от большего к меньшему, согласно ниже прописанному условию  
{
    return l.points > r.points;
}

int main() {
    system("chcp 1251 > nul");                  //возможность использования русского шрифта
    size_t n;                                   //для максимально допустимого значения введенных данных. я бы писал int тоже работает, но видимо тут сразу на миллион) 
    cout << "Введите количество игроков: ";
    cin >> n;
    Player* players = new Player[n];            //создание динамического одномерного массива player с указателем в класс (ничего себе так можно было?). 
    cout << "Введите данные игроков:\n";
    for (size_t i = 0; i < n; i++)              //цикл ручного ввода данных
    {
        string name;
        int point;
        cout << "Имя игрока " << i+1 << " (и его очки): ";
        cin >> name >> point;
        players[i].set_name(name);                          //запись данных в массив  
        players[i].set_points(point);                       //запись данных в массив
    }
    sort(players, players + n, point_sorter);               //метод сортировки из #include <algorithm>, заданного бинарным оператором bool point_sorter   
    for (size_t i = 0; i < n; i++)                          //вывод отсортированного массива
    {
        cout << players[i].get_name() << " " << players[i].get_points() << '\n';
    }
    delete[] players;                                          //очищение памяти динамического массива
}
